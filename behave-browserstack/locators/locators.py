from appium.webdriver.common.mobileby import MobileBy


class Locator(object):

    #Welcome screen

    welcometowheezo                     = 'title_text'


    # Home screen

    medication_btn                      = 'medication_card'
    logpreventer_btn                    = 'preventer_card'
    logreliever_btn                     = 'releiver_card'
    hamburgermenu                       = 'menu_button'
    hamburgermenu_id                    = 'Menu button'
    medication_btn_txt                  = 'Medication'



    # Hamburger menu

    signout_menu_txt                    = 'Sign out'
    signout_dialog_txt                  = 'Sign out'

    # Log preventer screen

    selectonemedicine                   = 'log_medication_medicine_text'
    list_popup                          = 'avatar_recycler_view'

    medicine                            = 'Select one'
    medicine_name                       = 'Alvesco Inhaler 160mcg'

    selectonedosage                     = 'log_medication_dosage_type_text'

    dosage                              = 'Select one'
    dosage_name                         = 'Pills'

    minus_btn                           = 'minus_button'
    dosage_default_logpreventer         = 'stepper_value'
    dosage_default_logpreventer_txt     = '1'

    time                                = 'log_medication_time_text'
    time_txt                            = 'Now'

    date_time_dialog_header             = 'title_text'
    date_time_dialog_header_txt         = 'Select date & time'

    done_dialog_header                  = 'done_button'
    done_dialog_header_txt              = 'Done'

    numberpicker_input                  ='numberpicker_input'
    date_numberpicker_input_txt         = '9'
    month_numberpicker_input_txt        = 'Jun'
    year_numberpicker_input_txt         = '1980'

    go_to_switch                        ='go_to_switch'
    go_to_switch_default_txt            = 'OFF'

    logapreventer_btn                   = 'delete_button'
    history_btn                         = 'history_card'
    preventerused                       = 'medication_text'
    preventerusedtxt_historyscreen      = 'Preventer used'
    preventerusedhistoryscreen_btn      = 'medication_text'
    share_lnk                           = 'share_text'
    share_txt                           = 'Share'

    # Preventer details

    preventerheader                      = 'header_title'
    preventerheader_txt                  = 'Preventer taken'

    timestamp                            = 'date_and_time_value'

    medicinepreventerscreen              = 'medicine_value'
    medicinevaluepreventerscreen_txt     = 'Alvesco Inhaler 160mcg'
    dosagepreventerscreen                = 'dosage_value'
    dosagevaluepreventerscreen_txt       ='999'
    dosagetype                           ='dosage_type_value'
    dosagetypepreventerscreen_txt        = 'Pills'
    deletepreventer_btn                  = 'delete_button'
    deletepreventer_btn_txt              = 'Delete log'

    delete_card_container                = 'card_container'
    deletedialogtitle                    = 'title_text'
    deletedialogtitletxt_popup           = 'Do you really want to delete the log?'
    deletedialogsubtitle                 = 'description_text'
    deletedialogsubtitletxt_popup        = 'You cannot retrieve this in the future.'
    deletedialog_delete_btn              = 'skip_button'
    deletedialog_deletebtn_txt           = 'Delete'
    deletedialog_cancel_btn              = 'cancel_button'
    deletedialog_cancelbtn_txt           = 'Cancel'

    back_btn                             = 'back_button'

    medicine_name_other                  = 'Other'


    # Sign-in

    signin_welcomescreen                 = 'sign_in_text'
    username                             = 'device@twobulls.com'
    password                             = 'twobulls'
    signin_button_txt                    = 'Sign In'


